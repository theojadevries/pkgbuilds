#!/bin/sh

# A script for updating Archlinux

cd ./source/
tar -zvcf hieroglyphThemes.tar.gz ./*
cd ..
mv ./source/hieroglyphThemes.tar.gz .
makepkg -g >> PKGBUILD
makepkg -f
sudo pacman -U python-hieroglyph-themes-0.02-1-any.pkg.tar.xz
