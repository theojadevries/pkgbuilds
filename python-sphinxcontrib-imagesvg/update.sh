#!/bin/sh

# A script for updating Archlinux

tar --exclude './.git' -zvcf sphinxcontrib-imagesvg.tar.gz -C ~/work/sphinxcontrib-imagesvg/ .
makepkg -g >> PKGBUILD
makepkg -f
sudo pacman -U python-sphinxcontrib-imagesvg-0.01-1-any.pkg.tar.xz
