#!/bin/sh

# A script for updating Archlinux

cd ./build/
tar -zvcf sphinxThemes.tar.gz ./*
cd ..
mv ./build/sphinxThemes.tar.gz .
makepkg -g >> PKGBUILD
makepkg -f
sudo pacman -U python-sphinx-themes-0.01-1-any.pkg.tar.xz 
sudo pacman -U python2-sphinx-themes-0.01-1-any.pkg.tar.xz 
